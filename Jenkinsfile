pipeline {
    agent any
    environment {
        AUTHOR = 'Antoine DEWERDT'
        PURPOSE    = 'This is a sample Django app'
        LIFE_QUOTE = 'The greatest glory in living lies not in never falling, but in rising every time we fall.'
    }
    stages {
        stage('Checkout') {
            steps {
                // Checkout your source code repository
                git branch: 'master',
                    url: 'https://gitlab.com/AntoineDew/dockerising_django_app.git'
            }
        }
        stage('Install dependencies') {
            steps {
                sh 'pip install -r requirements.txt'
            }
        }
        stage('Test') {
            steps {
                sh 'python3 manage.py test'
            }
        }
        stage('Build') {
            environment {
                DOCKER_CREDS = credentials('docker-harbor')
                COMMIT_SHA = """${sh(
                    returnStdout: true,
                    script: "git log -n 1 --pretty=format:'%H'"
                )}"""
            }
            steps {
                // Build your Django application
                sh 'docker build -t harbor.jonctions.com/library/antoine-dewerdt/django-simple-app:${COMMIT_SHA} .'
                sh 'docker login -u ${DOCKER_CREDS_USR} -p ${DOCKER_CREDS_PSW} harbor.jonctions.com'
                sh 'docker push harbor.jonctions.com/library/antoine-dewerdt/django-simple-app:${COMMIT_SHA}'
            }
        }
        stage('Deploy') {
            environment {
                COMMIT_SHA = """${sh(
                    returnStdout: true,
                    script: "git log -n 1 --pretty=format:'%H'"
                )}"""
            }
            steps {
                // Deploy your Django application
                sh 'docker rm -f django-sample-app || true'
                sh 'docker run --name django-sample-app -d -p 8000:8000 -it -e AUTHOR -e PURPOSE -e LIFE_QUOTE harbor.jonctions.com/library/antoine-dewerdt/django-simple-app:${COMMIT_SHA}'
            }
        }
    }
}
